@FCA_000004
Feature: test
  As a StackOverflow user or visitor
  I want to access the documentation section

  @FCA_000003
  Scenario: search documentation on Stack Overflow
    Given I am on StackOverflow
    And I go to the Documentation section
    When I search for "cucumber"
    And I follow the link to "cucumber"
    Then I should see documentation for "cucumber"
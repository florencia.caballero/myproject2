@LippiaTestManager @IssueReporter
Feature: Issue Reporter tests

  Background:
    Given I perform the Token Request and save the token

  @Regresion
  Scenario Outline: Obtengo los proyectos por token
    When Yo realizo una '<operation>' hacia '<entity>' endpoint con el '<jsonName>' y '<inputParameters>'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                                     | statusCode | operation | inputParameters                        | entity                       |
      | LippiaTestManager/IssueReporter/rq_get_projects_gitlab_token | 201        | POST      | gitlabToken:glpat-dtkmdq2UMiZbd3e_E6t5 | GETPROJECTSISSUEREPORTER_LTM |
      | LippiaTestManager/IssueReporter/rq_get_projects_jira_token   | 201        | POST      | jiraToken:L3VyVMajuwLoW0VpgMR2F540     | GETPROJECTSISSUEREPORTER_LTM |

  @Regresion
  Scenario Outline: Obtengo los tipos de issues
    When Yo realizo una '<operation>' hacia '<entity>' endpoint con el '<jsonName>' y '<inputParameters>'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                                   | statusCode | operation | inputParameters                    | entity                          |
      | LippiaTestManager/IssueReporter/rq_get_issues_types_gitlab | 201        | POST      |                                    | GETISSUESTYPESISSUEREPORTER_LTM |
      | LippiaTestManager/IssueReporter/rq_get_issues_types_jira   | 201        | POST      | jiraToken:L3VyVMajuwLoW0VpgMR2F540 | GETISSUESTYPESISSUEREPORTER_LTM |

  @Regresion @Smoke
  Scenario Outline: Se crea un issue para un proyecto
    When Yo realizo una '<operation>' hacia '<entity>' endpoint con el '<jsonName>' y '<inputParameters>'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                               | statusCode | operation | inputParameters                                                          | entity                       |
      | LippiaTestManager/IssueReporter/rq_create_issue_gitlab | 201        | POST      | gitlabToken:glpat-dtkmdq2UMiZbd3e_E6t5,type:issue,projectId:32929060     | CREATEISSUEISSUEREPORTER_LTM |
      | LippiaTestManager/IssueReporter/rq_create_issue_gitlab | 201        | POST      | gitlabToken:glpat-dtkmdq2UMiZbd3e_E6t5,type:incident,projectId:32929060  | CREATEISSUEISSUEREPORTER_LTM |
      | LippiaTestManager/IssueReporter/rq_create_issue_gitlab | 201        | POST      | gitlabToken:glpat-dtkmdq2UMiZbd3e_E6t5,type:test_case,projectId:32929060 | CREATEISSUEISSUEREPORTER_LTM |
      | LippiaTestManager/IssueReporter/rq_create_issue_jira   | 201        | POST      | jiraToken:L3VyVMajuwLoW0VpgMR2F540,type:10001,projectId:10000            | CREATEISSUEISSUEREPORTER_LTM |
      | LippiaTestManager/IssueReporter/rq_create_issue_jira   | 201        | POST      | jiraToken:L3VyVMajuwLoW0VpgMR2F540,type:10002,projectId:10000            | CREATEISSUEISSUEREPORTER_LTM |
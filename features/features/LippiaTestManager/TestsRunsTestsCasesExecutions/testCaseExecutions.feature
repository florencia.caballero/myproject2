@LippiaTestManager @TestCasesExecutions
Feature: Tests Cases Executions tests

  Background:
    Given I perform the Token Request and save the token

  @Smoke @Regresion
  Scenario Outline: Se obtiene un test case execution
    And I create a test run execution and save the runExecutionId
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testRunId,runExecutionId'
    Then I will get the proper status code '<statusCode>'
    Then I save the test case execution id as 'testCaseExecutionId'

    Examples:
      | jsonName                                                        | statusCode | operation | entity                  |
      | LippiaTestManager/TestCasesExecution/rq_get_test_case_execution | 200        | GET       | TESTCASESEXECUTIONS_LTM |


  @Smoke @Regresion
  Scenario Outline: Se agrega un attachment a un test case executions
    When Realizo un '<operation>' por Imagen con los '<inputParameters>' y obtiene el status '<statusCode>'
    Then I save the 'id' of the attachment as 'attachmentId'

    Examples:
      | operation | inputParameters   | statusCode |
      | POST      | fileName:test.png | 201        |

  @Regresion
  Scenario Outline: Se obtienen los attachments de un test case execution
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testCaseExecutionId'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                                                      | statusCode | operation       | entity                  |
      | LippiaTestManager/TestCasesExecution/rq_get_attachments_test_cases_executions | 200        | GET_ATTACHMENTS | TESTCASESEXECUTIONS_LTM |

  @Regresion @Smoke
  Scenario Outline: Se actualiza el estado de un test case execution
    When Yo realizo una '<operation>' hacia '<entity>' con el '<jsonName>' usando '<inputParameters>' y obteniendo los parametros: 'testCaseExecutionId'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                                             | statusCode | operation | inputParameters   | entity                  |
      | LippiaTestManager/TestCasesExecution/rq_update_test_cases_executions | 200        | PATCH     | status:PASSED     | TESTCASESEXECUTIONS_LTM |
#      | LippiaTestManager/TestCasesExecution/rq_update_test_cases_executions | 200        | PATCH     | status:UNEXECUTED | TESTCASESEXECUTIONS_LTM |

  @Regresion @Smoke
  Scenario Outline: Se elimina un attachment de un test case execution
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testCaseExecutionId,attachmentId'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                                                             | statusCode | operation | entity     |
      | LippiaTestManager/TestCasesExecution/rq_delete_attachment_from_test_cases_executions | 200        | DELETE    | DELETE_LTM |

  @Regresion @Smoke
  Scenario Outline: Se elimina un test case execution por el id
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'duplicatedTestCaseExecutionId'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                                                   | statusCode | operation | entity     |
      | LippiaTestManager/TestCasesExecution/rq_delete_test_cases_executions_by_id | 200        | DELETE    | DELETE_LTM |

  @Regresion @Smoke
  Scenario Outline: Se elimina el proyecto y todo lo creado para la suite de test case executions
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'projectId'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                     | statusCode | operation | entity     |
      | LippiaTestManager/Projects/rq_delete_project | 200        | DELETE    | DELETE_LTM |
@LippiaTestManager @TestRuns
Feature: Tests Runs

  Background:
    Given I perform the Token Request and save the token

  @Regresion @Smoke
  Scenario Outline: Se crea un test run
    And I create a project and save the projectId
    And I create a test for the project and save the test case id as 'testCaseId'
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'projectId'
    Then I will get the proper status code '<statusCode>'
    And I save the following fields: 'id' as 'testRunId'

    Examples:
      | jsonName                                       | statusCode | operation | entity            |
      | LippiaTestManager/TestsRuns/rq_create_test_run | 201        | POST      | CREATETESTRUN_LTM |

  @Regresion @Smoke
  Scenario Outline: Se obtiene un test run
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testRunId'
    Then I will get the proper status code '<statusCode>'
    And I save the execution id as 'runExecutionId'

    Examples:
      | jsonName                                    | statusCode | operation | entity         |
      | LippiaTestManager/TestsRuns/rq_get_test_run | 200        | GET       | GETTESTRUN_LTM |

  @Regresion
  Scenario Outline: Se obtienen test runs
    When Yo realizo una '<operation>' hacia '<entity>' endpoint con el '<jsonName>' y ''
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                     | statusCode | operation | entity         |
      | LippiaTestManager/TestsRuns/rq_get_test_runs | 200        | GET_LIST  | GETTESTRUN_LTM |

  @Regresion
  Scenario Outline: Se obtiene la lista de los test runs de un proyecto
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'projectId'
    Then I will get the proper status code '<statusCode>'
    Examples:
      | jsonName                                          | statusCode | operation         | entity         |
      | LippiaTestManager/TestsRuns/rq_get_list_test_runs | 200        | GET_LIST_PROJECTS | GETTESTRUN_LTM |

  @Regresion @Smoke
  Scenario Outline: Se agrega un Test Case a un test run
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testRunId,testCaseId'
    Then I will get the proper status code '<statusCode>'
    And I save the test case id as 'testCaseId'

    Examples:
      | jsonName                                                 | statusCode | operation | entity                   |
      | LippiaTestManager/TestsRuns/rq_add_test_case_to_test_run | 200        | PUT       | ADDTESTCASETOTESTRUN_LTM |

  @Regresion @Smoke
  Scenario Outline: Se duplica un test run
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testRunId'
    Then I will get the proper status code '<statusCode>'
    And I save the following fields: 'id' as 'duplicatedTestRunId'

    Examples:
      | jsonName                                                 | statusCode | operation | entity         |
      | LippiaTestManager/TestsRuns/rq_create_test_run_duplicate | 201        | POST      | GETTESTRUN_LTM |

  @Regresion @Smoke
  Scenario Outline: Se crea tags para el testRun por id
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testRunId'
    Then I will get the proper status code '<statusCode>'
    And I save the following fields: 'id' as 'tagId'

    Examples:
      | jsonName                                           | statusCode | operation | entity        |
      | LippiaTestManager/TestsRuns/rq_create_test_run_tag | 200        | PUT       | CREATETAG_LTM |

  @Regresion @Smoke
  Scenario Outline: Se elimina un tag de un test run
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testRunId,tagId'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                           | statusCode | operation | entity     |
      | LippiaTestManager/TestsRuns/rq_delete_test_run_tag | 200        | DELETE    | DELETE_LTM |

  @Regresion @Smoke
  Scenario Outline: Se crea la ejecucion de un test run
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'testRunId,runExecutionId'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                                  | statusCode | operation | entity                     |
      | LippiaTestManager/TestsRuns/rq_create_test_run_executions | 200        | PATCH     | CREATETESTRUNEXECUTION_LTM |

  @Regresion @Smoke
  Scenario Outline: Se elimina un test case de un run
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: 'duplicatedTestRunId,testCaseId'
    Then I will get the proper status code '<statusCode>'
    Examples:
      | jsonName                                                                 | statusCode | operation | entity     |
      | LippiaTestManager/TestsRuns/rq_delete_test_case_from_duplicated_test_run | 200        | DELETE    | DELETE_LTM |

  @Regresion @Smoke
  Scenario Outline: Se elimina un test run
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: '<field>'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                                 | statusCode | operation | field               | entity     |
      | LippiaTestManager/TestsRuns/rq_delete_test_run           | 200        | DELETE    | testRunId           | DELETE_LTM |
      | LippiaTestManager/TestsRuns/rq_delete_duplicate_test_run | 200        | DELETE    | duplicatedTestRunId | DELETE_LTM |

  @Regresion @Smoke
  Scenario Outline: Se elimina el proyecto y todo lo creado para la suite de test run
    When Realizo un '<operation>' hacia '<entity>' con el json '<jsonName>' obteniendo: '<parameters>'
    Then I will get the proper status code '<statusCode>'

    Examples:
      | jsonName                                         | statusCode | operation | entity     | parameters |
      | LippiaTestManager/Projects/rq_delete_project     | 200        | DELETE    | DELETE_LTM | projectId  |

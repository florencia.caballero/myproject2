@FCA_000008
Feature: addfile 2
  As a StackOverflow user or visitor
  I want to access the documentation section

  @FCA_000007
  Scenario: search documentation on Stack Overflow
    Given I am on StackOverflow
    And I go to the Documentation section
    When I search for "cucumber"
    And I follow the link to "cucumber"
    Then I should see documentation for "cucumber"